<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
* Auth Routes
*/

Route::group(['prefix' => 'v1'], function () {

    Route::group(['prefix' => 'auth'], function () {
        Route::post('/login', 'Api\AuthController@login');
    });

    Route::group(['prefix' => 'auth'], function () {
        Route::middleware(['auth:api'])->post('/logout', 'Api\AuthController@logout');
    });

});

/*
* Companies Routes
*/

Route::group(['prefix' => 'v1'], function () {

    Route::group(['prefix' => 'company'], function () {

        Route::middleware(['auth:api', 'role:admin'])
            ->post('/register', 'Api\CompanyController@store');

        Route::middleware(['auth:api', 'role:admin'])
            ->get('/list', 'Api\CompanyController@index');

        Route::middleware(['auth:api', 'role:admin'])
            ->put('/update/{company_id}', 'Api\CompanyController@update');

        Route::middleware(['auth:api', 'role:admin'])
            ->delete('/delete/{company_id}', 'Api\CompanyController@destroy');
    });

});
