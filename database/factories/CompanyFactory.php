<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Entities\Company;
use Faker\Generator as Faker;

$factory->define(Company::class, function (Faker $faker) {
    return [
        'name' => $faker->name(),
        'email' => $faker->email(),
        'website' => $faker->url(),
        'logo' => $faker->imageUrl($width = 640, $height = 480),
    ];
});
