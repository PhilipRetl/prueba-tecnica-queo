<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Entities\User;

class Employee extends Model{

    protected $table = 'employees';
    protected $fillable = [
        'user_id'
    ];
    protected $guarded = [
        'company_id',
        'user_id'
    ];

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function company(){
        return $this->belongsTo(Company::class, 'company_id');
    }
}

