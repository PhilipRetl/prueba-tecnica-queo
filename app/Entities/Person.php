<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Entities\User;

class Person extends Model{

    protected $table='persons';
    protected $fillable=[
        'name',
        'last_name',
        'phone'
    ];
    protected $guarded=[
        'user_id'
    ];

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }
}
