<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Entities\Employee;
use App\Entities\Company;

class Company extends Model{

    protected $table = 'companies';
    protected $fillable = [
        'name',
        'email',
        'website',
        'logo'
    ];

    public function employees(){
        return $this->hasMany(Employee::class, 'company_id');
    }

    public function administrator(){
        return $this->hasOne(Administrator::class, 'company_id');
    }
}
