<?php

namespace App\Services;

use Venoudev\Results\Result;
use App\Services\Contracts\CompanyService;
use App\Validators\RegisterCompanyValidator;
use App\Validators\UpdateCompanyValidator;
use App\Actions\Company\RegisterCompanyAction;
use App\Actions\Company\ListCompanyAction;
use App\Actions\Company\UpdateCompanyAction;
use App\Actions\Shared\DeleteAction;
use App\Entities\Company;

class CompanyServiceImpl implements CompanyService{


    public function listCompany($result):Result{

        ListCompanyAction::execute($result);

        return $result;

    }

    public function registerCompany($data, $result):Result{

        RegisterCompanyValidator::execute($data, $result);

        if($result->findMessage('[ERR_CHECK_DATA]')){
            return $result;
        }

        RegisterCompanyAction::execute($data, $result);

        if($result->findMessage('[FAILED_CREATED]')==false){
            return $result;
        }

        $result->setMessages([]);
        $result->setStatus('success');
        $result->addMessage('[COMPANY_CREATED] # Company created correctly');

    }

    public function updateCompany($data, $company_id, $result):Result{

        UpdateCompanyValidator::execute($data, $company_id,$result);

        if($result->findMessage('[ERR_CHECK_DATA]')){
            return $result;
        }

        if($result->findMessage('[NOT_FOUND]')){
            return $result;
        }

        UpdateCompanyAction::execute($data, $company_id, $result);

        if($result->findMessage('[FAILED_UPDATED]')==false){
            return $result;
        }

        $result->setMessages([]);
        $result->setStatus('success');
        $result->addMessage('[COMPANY_UPDATED] # Company updated correctly');

    }

    public function deleteCompany($company_id, $result):Result{

        $model = Company::class;

        DeleteAction::execute($model, $company_id, $result);

        if($result->findMessage('[NOT_FOUND]')){
            return $result;
        }

        if($result->findMessage('[FAILED_DELETED]')){
            return $result;
        }

        return $result;

    }


}
