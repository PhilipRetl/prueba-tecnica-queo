<?php

namespace App\Services;

use App\Services\Contracts\AuthService;
use Venoudev\Results\Result;
use App\Validators\LoginValidator;
use App\Actions\Auth\LoginAction;
use App\Actions\Auth\LogoutAction;

class AuthServiceImpl implements AuthService{

    public function login($data, $result):Result{

        LoginValidator::execute($data, $result);

        if($result->findMessage('[ERR_CHECK_DATA]')){
           return $result;
        }

        LoginAction::execute($data, $result);


        if($result->findMessage('[FAILED_AUTH]')){
            $result->setMessages([]);
            $result->addMessage('[FAILED_AUTH] # Invalid login credential');
            return $result;
        }

        $result->setMessages([]);
        $result->setStatus('success');
        $result->addMessage('[LOGIN_SUCCESS] # login do correctly');

        return $result;

    }

    public function logout( $result):Result{

        LogoutAction::execute($result);

        if($result->findMessage('[LOGOUT]')==false){
            $result->setMessages([]);
            $result->setStatus('fail');
            $result->addMessage('[LOGOUT_FAIL] # logout error try again later');
            return $result;
        }

        return $result;

    }
}
