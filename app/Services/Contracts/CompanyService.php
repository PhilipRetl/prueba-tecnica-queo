<?php

namespace App\Services\Contracts;

use Venoudev\Results\Result;

interface CompanyService{

    public function registerCompany($data, $result):Result;
    public function listCompany($result):Result;
    public function updateCompany($data, $company_id, $result):Result;
    public function deleteCompany($company_id, $result):Result;

}
