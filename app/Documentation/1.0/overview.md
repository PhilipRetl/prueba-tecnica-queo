<p align="center"><a href="https://queoaccess.com"><img src="https://appprueba.venoudev.com/img/queo.png" width="200" alt="Queo App"></a>
</p>


# API Docs

<larecipe-card shadow>
    Welcome to the QueoAccess API Documentation
</larecipe-card>

At [Venoudev](https://venoudev.com) uses laracipe for document our API Rest

|Json Response Structure|
|:-|

```json
{
    "success": bool ,
    "description": "string",
    "data": { JsonObject{} || JsonArrayObject[] },
    "errors": [
        {
            "code_message": "[CODE]",
            "field": "[FIELD || NOTHING]",
            "message": "string"
        }
    ],
    "messages": [
        {
            "code_message": "[CODE]",
            "message": "string"
        }
    ]
}
```

> {info} Message Object

#### Example
```json
    {
        "code_message": "[LOGIN_SUCCESS]",
        "message": "Login do correctly"
    }

```

> {danger} Error Object

#### Example
```json

    {
        "code_message": "[ERR_REQUIRED]",
        "field": "[email]",
        "message": "The email field is required."
    }

```

