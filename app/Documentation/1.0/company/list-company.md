<p align="center"><a href="https://queoaccess.com"><img src="https://appprueba.venoudev.com/img/queo.png" width="200" alt="Queo App"></a>
</p>


# API Docs

At [Venoudev](https://venoudev.com) uses laracipe for document our API Rest

<a name="login"></a>

## List Companies

List of Companies registered in QueoAccessApp

### Endpoint

|Method|URI|
|:-|:-|:-|
|GET|`/api/v1/company/list`|

|Headers|
|:-|
|[`Accept: application/json`]|
|[`Authorization: Bearer [your_token]`]|

### Role

```text
admin
```

### Authorization 

#### Example

```json
{
    "Authorization": "Bearer  eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiMjAxZWIzN2IyMTVkNTc5OWU0ZDg2NjJlNTRkYWM5OWEyNTYzMWE3OWM4MWZiMGEzNmRkMDY3NzdlN2M3ZTllZjYzMTA1ZjNiZmYwMzgxOWQiLCJpYXQiOjE1OTI2OTU4ODYsIm5iZiI6MTU5MjY5NTg4NiwiZXhwIjoxNjI0MjMxODg2LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.JgK84-CDMJ6xOQlmpnSdvtTGu0to0mDi0tjY6JhZCeGrfWSWb23SEf3rNDKbtWWiSAp3yBnP08v9J9GYrMwtx9ItYoANGn8qjSr2GVep2bK9GjjkErOkDWOIXeEw7tPxD5KD4xWXKY6_uiGX3Jj5m6EbhFsxzj1q1nIpJtGBxkVNQvg1fDtUGjc2qA5aFiqjRGDajFTPMyojyTOvf-tKhid_RWupdz5H4fBBjODMCAw4XBmqRhvT6WHv0WWAyvwoJCzAQTWqiEpctqthc-0HzpGTBxuqsdj71poowFJMtnL6r6_AYsEOn2IrDsR8tNjIBQ05iDrM6KZkHTuHVsKPo7augrwf6glpsuiASuy4Au1VlwJVUfno3xjCTcX7vsNzvqVSb6E2_0FWTTMwSHqkWQQNfI03daDOFyVej69U_4DqbN_cvcl9rZJp-WYXiB3C89Za1UwSxp8Ff9xcYowrw8vwb0PHvnPpkMTeHAnS59zLQrl5R-fqh-PKJe0gACK3W5-weJqoyE7_B-FziFqZdRhm7zwvSEZW2myEFdNOiUBeJ7OUV81a5CP1Gt7C0n5ejQhoPN5s60qHcSiYQFaKuhI_6rWLW9bNlFSqzHTA1DHYFFBQg4j6Vx-EqaAuZGw_cCYZMpF95A8C9_kLtjh1ayHhKae773BCulf_1ZEAYE"
}
```
> {success} Success Response

Code `200` `Ok`

Content

```json
{
    "success": true,
    "description": "Company list of QueoApp",
    "data": [
        {
            "companies_paginated": [
                {
                    "name": "Mr. Mariano Balistreri",
                    "email": "lmorar@gmail.com",
                    "website": "http://www.sawayn.biz/saepe-consequatur-dolorum-est-nulla",
                    "logo": "https://lorempixel.com/640/480/?47346"
                },
                {
                    "name": "Margot Doyle",
                    "email": "ryan.rath@von.info",
                    "website": "http://hill.com/unde-et-omnis-sit-nam",
                    "logo": "https://lorempixel.com/640/480/?13709"
                },
            ],
            "total": 25,
            "to": 15,
            "prev_page_url": null,
            "per_page": 15,
            "path": "http://prueba_tecnica_queo.test/api/v1/company/list",
            "next_page_url": "http://prueba_tecnica_queo.test/api/v1/company/list?page=2",
            "last_page_url": "http://prueba_tecnica_queo.test/api/v1/company/list?page=2",
            "last_page": 2,
            "from": 1,
            "first_page_url": "http://prueba_tecnica_queo.test/api/v1/company/list?page=1",
            "current_page": 1
        }
    ],
    "errors": [],
    "messages": [
        {
            "code_message": "[LIST_DATA_PAGINATED]",
            "message": "Type of list on JsonResponse data field"
        },
        {
            "code_message": "[COMPANIES]",
            "message": "Type of data on JsonResponse data field"
        }
    ]
}
```

### Messages

<larecipe-badge type="info" circle icon="fa fa-commenting-o"></larecipe-badge> 

|code_message|message|
|:-|:-|
|`[LIST_DATA_PAGINATED]`|`Type of list on JsonResponse data field`|
|`[EMPTY]`|`Empty list of Companies registred in QueoApp`|
|`[COMPANIES] `|`Type of data on JsonResponse data field'`|

