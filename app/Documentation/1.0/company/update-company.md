<p align="center"><a href="https://queoaccess.com"><img src="https://appprueba.venoudev.com/img/queo.png" width="200" alt="Queo App"></a>
</p>


# API Docs

At [Venoudev](https://venoudev.com) uses laracipe for document our API Rest

<a name="login"></a>

## Updata Company

Update company data in QueoAccessApp for the file field logo this route requires that petition be post with the field 
_method = PUT 

### Endpoint

|Method|URI|
|:-|:-|:-|
|POST|`/api/v1/company/update/{company_id}`|

|Headers|
|:-|
|[`Accept: application/json`]|
|[`Authorization: Bearer [your_token]`]|

### Role

```text
admin
```

### Authorization 

#### Example

```json
{
    "Authorization": "Bearer  eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiMjAxZWIzN2IyMTVkNTc5OWU0ZDg2NjJlNTRkYWM5OWEyNTYzMWE3OWM4MWZiMGEzNmRkMDY3NzdlN2M3ZTllZjYzMTA1ZjNiZmYwMzgxOWQiLCJpYXQiOjE1OTI2OTU4ODYsIm5iZiI6MTU5MjY5NTg4NiwiZXhwIjoxNjI0MjMxODg2LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.JgK84-CDMJ6xOQlmpnSdvtTGu0to0mDi0tjY6JhZCeGrfWSWb23SEf3rNDKbtWWiSAp3yBnP08v9J9GYrMwtx9ItYoANGn8qjSr2GVep2bK9GjjkErOkDWOIXeEw7tPxD5KD4xWXKY6_uiGX3Jj5m6EbhFsxzj1q1nIpJtGBxkVNQvg1fDtUGjc2qA5aFiqjRGDajFTPMyojyTOvf-tKhid_RWupdz5H4fBBjODMCAw4XBmqRhvT6WHv0WWAyvwoJCzAQTWqiEpctqthc-0HzpGTBxuqsdj71poowFJMtnL6r6_AYsEOn2IrDsR8tNjIBQ05iDrM6KZkHTuHVsKPo7augrwf6glpsuiASuy4Au1VlwJVUfno3xjCTcX7vsNzvqVSb6E2_0FWTTMwSHqkWQQNfI03daDOFyVej69U_4DqbN_cvcl9rZJp-WYXiB3C89Za1UwSxp8Ff9xcYowrw8vwb0PHvnPpkMTeHAnS59zLQrl5R-fqh-PKJe0gACK3W5-weJqoyE7_B-FziFqZdRhm7zwvSEZW2myEFdNOiUBeJ7OUV81a5CP1Gt7C0n5ejQhoPN5s60qHcSiYQFaKuhI_6rWLW9bNlFSqzHTA1DHYFFBQg4j6Vx-EqaAuZGw_cCYZMpF95A8C9_kLtjh1ayHhKae773BCulf_1ZEAYE"
}
```
### Body Data

```json
{
    "_method"   :   "PUT",     
    "name"      :   "string",
    "email"     :   "email",
    "logo"      :   "file",
    "website"   :   "url",
}
```

### Example 

```json
{   
    "_method"   :   "PUT",
    "name"      :   "Queo",
    "email"     :   "support@company.com",
    "logo"      :   "logoQueo.png",
    "website"   :   "https://www.queo.com",
}
```

> {success} Success Response

Code `200` `Ok`

Content

```json
{
    "success": true,
    "description": "Company update succesfuly",
    "data": {
        "id": 26,
        "name": "Queo",
        "email": "support@queo.com",
        "website": "https://www.queo.com",
        "logo": "https://lorempixel.com/640/480/?95727"
    },
    "errors": [],
    "messages": [
        {
            "code_message": "[UPDATED]",
            "message": "The company was updated"
        }
    ]
}
```
> {danger} Error Response

Code `400` `Bad Request`

### Messages

<larecipe-badge type="info" circle icon="fa fa-commenting-o"></larecipe-badge> 

|code_message|message|
|:-|:-|
|`[NOT_FOUND]`|The company don't exists|
|`[ERR_CHECK_DATA]`|The form has errors whit the inputs.|

### Errors

<larecipe-badge type="danger" circle icon="fa fa-exclamation-triangle"></larecipe-badge> 

|code_message|field|message|
|:-|:-|:-|
|`[ERR_REQUIRED]`|`[email]`|The email field is required.|
|`[ERR_REQUIRED]`|`[name]`|The password field is required.|
|`[ERR_REQUIRED]`|`[website]`|The password field is required.|
|`[ERR_UNIQUE]`|`[email]`|The email has already been taken.| 
|`[ERR_URL]`|`[website]`|The website format is invalid.|
|`[ERR_FILE]`|`[logo]`|The logo must be a file.|

> {danger} Error Response

Code `409` `Conflict`

### Messages

<larecipe-badge type="info" circle icon="fa fa-commenting-o"></larecipe-badge> 

|code_message|message|
|:-|:-|
|`[FAILED_UPDATED]`|The company was not updated try again.|

