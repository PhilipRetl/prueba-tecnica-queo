<?php

namespace App\Documentation\Api;

/**
 * @OA\Post(
 *     path="/api/v1/auht/login",
 *     tags={"auth"},
 *     summary="Gives Oauth2 credentials to use Queo Api ",
 *     operationId="authLogin",
 *
 *     @OA\Parameter(
 *         name="Accept",
 *         in="header",
 *         description="application/json",
 *         required=true,
 *         explode=true,
 *     ),
 *
 *     @OA\Parameter(
 *         name="Authorization",
 *         in="header",
 *         description="Bearer [token]",
 *         required=true,
 *         explode=true,
 *     ),
 *
 *     security={
 *       {"passport": {}},
 *     },
 *
 *     @OA\Parameter(
 *         name="Content-Type",
 *         in="header",
 *         description="application/x-www-form-urlencoded",
 *         required=true,
 *         explode=true,
 *     ),
 *
 *    @OA\Parameter(
 *         name="email",
 *         in="query",
 *         description="User email",
 *         required=true,
 *         explode=true,
 *    ),
 *
 *     @OA\Parameter(
*          name="password",
*         in="query",
*         description="Password gives to user for authenticate on Queo Api",
*         required=false,
*         explode=true,
 *
 *    ),
 *
 *    @OA\Response(
 *         response=400,
 *         description="Error en los datos"
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Empleado agregado correctamente",
 *
 *     ),
 *
 * )
 * @OA\Post(
 *     path="/api/v1/auht/logout",
 *     tags={"auth"},
 *     summary="Dispose Oauth2 credentials to don't use Queo Api ",
 *     operationId="authLogout",
 *
 *     @OA\Parameter(
 *         name="Accept",
 *         in="header",
 *         description="application/json",
 *         required=true,
 *         explode=true,
 *     ),
 *     @OA\Parameter(
 *         name="Authorization",
 *         in="header",
 *         description="Bearer [token]",
 *         required=true,
 *         explode=true,
 *     ),
 *     security={
 *       {"passport": {}},
 *     },
 *    @OA\Response(
 *         response=400,
 *         description="Error en los datos"
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Empleado agregado correctamente",
 *
 *     ),
 *  )
 *
 */
