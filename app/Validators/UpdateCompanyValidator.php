<?php

namespace App\Validators;

use VenouDev\Results\Result;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Entities\Company;

class UpdateCompanyValidator
{

    public static function execute($data, $company_id, Result $result){

        $company = Company::find($company_id);

        if($company==null){

            $result->setCode(400);
            $result->setStatus('fail');
            $result->addMessage('[NOT_FOUND] # The company don\'t exists');

            return $result;
        }

        $validator=Validator::make($data,[
            'name'=> ['required', 'string', 'max:100'],
            'email'=> ['email', 'required', 'string', 'max:100',
                Rule::unique('companies', 'email')->ignore($company->id),
            ],
            'website'=> ['url', 'required', 'string', 'max:100'],
            'logo' => ['file'],

        ]);

        if ($validator->fails()) {

            $result->setCode(400);
            $result->setStatus('fail');
            $result->setErrors($validator->errors());
            $result->addMessage('[ERR_CHECK_DATA] # The form has errors whit the inputs');

            return $result;
        }

        $result->addMessage('[VALIDATED] # login inputs validated');

        return $result;


    }

}
