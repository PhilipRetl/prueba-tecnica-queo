<?php

namespace App\Validators;

use VenouDev\Results\Result;
use Illuminate\Support\Facades\Validator;

class RegisterCompanyValidator
{

    public static function execute($data, Result $result){

        $validator=Validator::make($data,[
          'name'=> ['required', 'string', 'max:100'],
          'email'=> ['email', 'required', 'string', 'max:100', 'unique:companies'],
          'website'=> ['url', 'required', 'string', 'max:100'],
          'logo' => ['file'],

        ]);

        if ($validator->fails()) {

            $result->setCode(400);
            $result->setStatus('fail');
            $result->setErrors($validator->errors());
            $result->addMessage('[ERR_CHECK_DATA] # The form has errors whit the inputs');

            return $result;
        }

        $result->addMessage('[VALIDATED] # login inputs validated');

        return $result;


    }

}
