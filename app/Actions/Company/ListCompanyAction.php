<?php

namespace App\Actions\Company;

use Venoudev\Results\Result;
use App\Entities\Company;

class ListCompanyAction{

    public static function execute($result):Result{

        $companies = Company::orderBy('id','desc')->get();

        if($companies->isEmpty()){
            $result->setMessages([]);
            $result->setCode(200);
            $result->addDatum('[COMPANIES]',[]);
            $result->setStatus('success');
            $result->addMessage('[EMPTY] # Empty list of Companies registred in QueoApp');
            return $result;
        }

        $result->setMessages([]);
        $result->setCode(200);
        $result->setStatus('success');
        $result->addDatum('[COMPANIES]', $companies);
        $result->addMessage('[LIST_DATA_PAGINATED] # Type of list on JsonResponse data field');
        $result->addMessage('[COMPANIES] # Type of data on JsonResponse data field');
        return $result;
    }
}
