<?php

namespace App\Actions\Company;

use Venoudev\Results\Result;
use App\Entities\Company;

class RegisterCompanyAction{

    public static function execute($data, $result):Result{

        Company::create([
            'name'  => $data['name'],
            'email' => $data['email'],
            'website' => $data['website'],
        ]);

        $company = Company::where('email', $data['email'])->first();

        if($company==null){

            $result->setCode(409);
            $result->setStatus('fail');

            $result->addMessage('[NOT_REGISTER] # The company was not registered try again');
            return $result;
        }
        $result->setMessages([]);

        $result->setCode(200);

        $result->setStatus('success');

        $result->addDatum('[COMPANY]', $company);
        $result->addMessage('[REGISTERED] # The company was created');
        return $result;
    }
}
