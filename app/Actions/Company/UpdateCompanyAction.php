<?php

namespace App\Actions\Company;

use Venoudev\Results\Result;
use App\Entities\Company;

class UpdateCompanyAction{

    public static function execute($data, $company_id ,$result):Result{

        $company = Company::find($company_id);

        $company->fill($data);
        $company->save();

        $company = Company::where('email', $data['email'])->first();

        if($company==null){

            $result->setCode(409);
            $result->setStatus('fail');

            $result->addMessage('[FAILED_UPDATED] # The company was not updated try again');
            return $result;
        }
        $result->setMessages([]);

        $result->setCode(200);
        $result->setStatus('success');

        $result->addDatum('[COMPANY]', $company);
        $result->addMessage('[UPDATED] # The company was updated');

        return $result;
    }
}
