<?php

namespace App\Actions\Shared;

use Venoudev\Results\Result;

class DeleteAction{

    public static function execute($model, $model_id ,$result):Result{

        $arrayName=explode("\\",$model);

        $modelName = strtolower(end($arrayName));

        $entity = $model::find($model_id);

        if($entity==null){

            $result->setCode(400);
            $result->setStatus('fail');
            $result->addMessage('[NOT_FOUND] # The ' .$modelName. ' don\'t exists');

            return $result;
        }

        $entity->delete();

        $entity = $model::find($model_id);

        if($entity != null){

            $result->setCode(409);
            $result->setStatus('fail');

            $result->addMessage('[FAILED_DELETED] # The ' .$modelName. ' was not deleted try again');
            return $result;
        }

        $result->setMessages([]);

        $result->setCode(200);
        $result->setStatus('success');
        $result->addMessage('[DELETED] # The ' .$modelName. ' was deleted');

        return $result;
    }
}
