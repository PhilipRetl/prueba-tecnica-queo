<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\AuthServiceImpl;
use App\Services\Contracts\AuthService;
use App\Services\CompanyServiceImpl;
use App\Services\Contracts\CompanyService;

class SourcesServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(AuthService::class, AuthServiceImpl::class);
        $this->app->bind(CompanyService::class, CompanyServiceImpl::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
