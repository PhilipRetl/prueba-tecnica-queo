<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Contracts\CompanyService;
use Venoudev\Results\Result;
use App\Http\Resources\CompanyResource;
use Venoudev\Results\ApiJsonResources\PaginateResource;

class CompanyController extends Controller
{

    private $companyService;

    public function __construct(CompanyService $service){
        $this->companyService = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = new Result();

        $this->companyService->listCompany($result);

        switch ($result->getStatus()) {
            case 'success':
                $data = CompanyResource::collection($result->getDatum('[COMPANIES]'));
                return $this->successResponse(
                    PaginateResource::make($data->paginate(15),'companies_paginated'),
                    $result->getMessages(),
                    200,
                    'Company list of QueoApp'
                );

                break;

            case 'fail' :

                return $response = $this->errorResponse(
                    $result->getErrors(),
                    $result->getMessages(),
                    $result->getCode(),
                    'exist conflict whit the request, please check the errors and messages'
                );

                break;

            break;

            default:
                return $response= $this->errorResponse([],[], 500,
                    'Unhandled case, please contact with the administrator');
            break;
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){

        $data= $request->only(['name', 'email', 'website', 'logo']);;

        $result = new Result();

        $this->companyService->registerCompany($data, $result);

        switch ($result->getStatus()) {
            case 'success':

                return $this->successResponse(
                    CompanyResource::make($result->getDatum('[COMPANY]')),
                    $result->getMessages(),
                    200,
                    'Company registered succesfuly'
                );

                break;

            case 'fail' :

                return $response= $this->errorResponse(
                    $result->getErrors(),
                    $result->getMessages(),
                    $result->getCode(),
                    'exist conflict whit the request, please check the errors and messages'
                );

                break;

            break;

            default:
                return $response= $this->errorResponse([],[], 500,
                    'Unhandled case, please contact with the administrator');
            break;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){

        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){

        $data= $request->only(['name', 'email', 'website', 'logo']);

        $result = new Result();

        $this->companyService->updateCompany($data, $id, $result);

        switch ($result->getStatus()) {
            case 'success':

                return $this->successResponse(
                    CompanyResource::make($result->getDatum('[COMPANY]')),
                    $result->getMessages(),
                    200,
                    'Company update succesfuly'
                );

                break;

            case 'fail' :

                return $response= $this->errorResponse(
                    $result->getErrors(),
                    $result->getMessages(),
                    $result->getCode(),
                    'Exist conflict whit the request, please check the errors and messages'
                );

                break;

            break;

            default:
                return $response= $this->errorResponse([],[], 500,
                    'Unhandled case, please contact with the administrator');
            break;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = new Result();

        $this->companyService->deleteCompany($id, $result);

        switch ($result->getStatus()) {
            case 'success':
                return $this->successResponse(
                    [],
                    $result->getMessages(),
                    200,
                    'Company deleted succesfuly'
                );

                break;

            case 'fail' :

                return $response= $this->errorResponse(
                    $result->getErrors(),
                    $result->getMessages(),
                    $result->getCode(),
                    'Exist conflict whit the request, please check the errors and messages'
                );

                break;

            break;

            default:
                return $response= $this->errorResponse([],[], 500,
                    'Unhandled case, please contact with the administrator');
            break;
        }
    }
}
